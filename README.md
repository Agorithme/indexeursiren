# IndexeurSiren

Ce projet implémente un système d'indexation automatisé pour importer des données à partir de fichiers CSV dans une base de données MongoDB en utilisant des workers gérés par PM2. Ce système est conçu pour s'exécuter sur un environnement Ubuntu.

## Fonctionnalités

- Téléchargement et décomposition automatiques de fichiers CSV.
- Indexation des données dans MongoDB via un cluster de workers.
- Utilisation de PM2 pour une gestion optimisée des processus.

## Prérequis

Avant de commencer, assurez-vous que les éléments suivants sont installés sur votre système :
- Node.js (version recommandée 16.x ou supérieure)
- MongoDB (local ou distant)
- PM2
- npm (gestionnaire de paquets pour Node.js)

## Installation

Clonez le dépôt sur votre machine locale avec la commande suivante :

```bash
git clone https://gitlab.com/Agorithme/indexeursiren.git
cd your-project-directory

Installez les dépendances requises via npm : 

npm install

Vérifiez que l'instance MongoDB est en cours d'exécution et accessible.

## Configuration

Configurez votre environnement en créant un fichier .env à la racine de votre projet avec les informations suivantes :

MONGO_URI=mongodb+srv://your-mongo-credentials

## Démarrage de l'application 

Pour lancer l'indexeur, utilisez la commande suivante :

pm2 start process.json

Cela va démarrer le controller qui orchestrera la distribution des fichiers CSV aux workers pour l'indexation.

## Structure des Fichiers
- process.json : Configuration PM2 pour lancer le controller.
- controller.js : Script pour distribuer les tâches d'indexation aux workers.
- worker.js : Script de worker pour traiter les fichiers CSV et les insérer dans MongoDB.