const pm2 = require('pm2');
const fs = require('fs');
const path = require('path');

let workerStatus = []; //suivre l'état des workers

pm2.connect((err) => {
  if (err) {
    console.error(err);
    process.exit(2);
  }

  pm2.start({
    name: 'Worker',
    script: 'worker.js',
    instances: 'max',
    max_memory_restart: '100M',
    env: {
      NODE_ENV: 'development'
    }
  }, (err) => {
    if (err) {
      console.error(err);
      return;
    }
    pm2.launchBus((err, bus) => {
      if (err) {
        console.error('Failed to launch PM2 message bus:', err);
        return;
      }
      bus.on('process:msg', (packet) => {
        if (packet.data && packet.data.done) {
          logWithTimestamp(`Worker has finished processing: ${packet.data.filePath}`);
          freeWorker(packet.process.pm_id);
        }
      });
    });

    pm2.list((err, updatedList) => {
      if (err) {
        console.error("Erreur lors de la récupération de la liste des processus après démarrage:", err);
        return;
      }
      workerStatus = updatedList.filter(app => app.name === 'Worker').map(() => false);
      distributeFiles();
    });
  });
});

function findAvailableWorker() {
  for (let i = 0; i < workerStatus.length; i++) {
    if (!workerStatus[i]) {
      workerStatus[i] = true; // Marquer comme occupé
      return i;
    }
  }
  return null;
}

function freeWorker(index) {
  if (index !== null && index < workerStatus.length) {
    workerStatus[index] = false;
  }
}

function distributeFiles() {
  const dir = path.join(__dirname, 'splitResult');
  fs.readdir(dir, (err, files) => {
    if (err) {
      logWithTimestamp(`Error reading directory: ${err}`);
      return;
    }

    files.forEach(file => {
      let workerId = findAvailableWorker();
      if (workerId !== null) {
        pm2.sendDataToProcessId(workerId, {
          type: 'process:msg',
          data: { filePath: path.join(dir, file) },
          topic: 'Processing CSV'
        }, (err, res) => {
          if (err) logWithTimestamp(`Error sending data to worker ${workerId}: ${err}`);
          else logWithTimestamp(`File ${file} sent to worker ${workerId}`);
        });
      }
    });
  });
}

function logWithTimestamp(message) {
  const timestamp = new Date().toLocaleString();
  console.log(`${timestamp} | ${message}`);
}
