const mongoose = require('mongoose');
const csv = require('csv-parser');
const fs = require('fs');
require('dotenv').config();

// Connexion à MongoDB
mongoose.connect(process.env.Mongo_URI)
  .then(() => logWithTimestamp('Mongo Connected.'))
  .catch(err => {
    console.error('MongoDB connection error:', err);
    process.exit(1); 
  });

const EtablissementSchema = new mongoose.Schema({
  siren: String,
  nic: String,
  siret: String,
  dateCreationEtablissement: Date,
  dateDernierTraitementEtablissement: Date,
  typeVoieEtablissement: String,
  libelleVoieEtablissement: String,
  codePostalEtablissement: String,
  libelleCommuneEtablissement: String,
  codeCommuneEtablissement: String,
  dateDebut: Date,
  etatAdministratifEtablissement: String
});

const Etablissement = mongoose.model('Etablissement', EtablissementSchema);


const processFile = async (filePath) => {
  let bulkOps = [];

  const stream = fs.createReadStream(filePath)
    .pipe(csv())
    .on('data', (row) => {
      Object.keys(row).forEach(key => {
        if (row[key] === '' || row[key] === null) {
          delete row[key];
        }
      });

      bulkOps.push({
        insertOne: {
          document: new Etablissement(row)
        }
      });

      if (bulkOps.length >= 1000) {
        Etablissement.bulkWrite(bulkOps).catch(err => logWithTimestamp('Error: ' + err.message));
        bulkOps = [];
      }
    })
    .on('end', async () => {
      if (bulkOps.length > 0) {
        await Etablissement.bulkWrite(bulkOps).catch(err => logWithTimestamp('Error: ' + err.message));
      }
      logWithTimestamp(`Traitement terminé pour le fichier ${filePath}`);
      
      fs.unlink(filePath, (err) => {
        if (err) {
          logWithTimestamp(`Error deleting file ${filePath}: ${err}`);
        } else {
          logWithTimestamp(`File ${filePath} deleted successfully`);
        }
      });

      process.send({ done: true, filePath });
    });

    stream.on('error', (error) => {
      logWithTimestamp(`Error in file stream: ${filePath}, ${error.message}`);
    });
  };
  

  process.on('message', (message) => {
    logWithTimestamp('Received message: ' + JSON.stringify(message)); 
    console.log(message.data.filePath);
    const filePath = message.data.filePath;
    if (filePath) {
      logWithTimestamp(`Démarrage du traitement du fichier : ${filePath}`);
      processFile(filePath);
    } else {
      logWithTimestamp('Aucun fichier spécifié');
    }
  });

function logWithTimestamp(message) {
  const timestamp = new Date().toLocaleString(); // date et heure actuelles
  console.log(`${timestamp} | ${message}`);
}
