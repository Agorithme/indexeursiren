const fs = require('fs');
const csvSplitStream = require('csv-split-stream');

const inputFilePath = './StockEtablissement_utf8.csv';
const outputDir = './splitResult/';
const maxLines = 10000;

if (!fs.existsSync(outputDir)) {
  fs.mkdirSync(outputDir, { recursive: true });
}

csvSplitStream.split(
  fs.createReadStream(inputFilePath),
  {
    lineLimit: maxLines,
    destinationFolder: outputDir,
    fileNameTemplate: 'output-<%= index %>.csv',
    preserveHeader: true
  },
  (index) => fs.createWriteStream(`${outputDir}output-${index + 1}.csv`)
)
.then(results => {
  console.log('CSV split completed successfully:', results);
})
.catch(err => {
  console.error('Error during CSV split:', err);
});

